using CSharpConsoleRPG.Attributes;
using CSharpConsoleRPG.Characters;
using SemanticComparison;
using System;
using Xunit;

namespace CSharpConsoleRPGTests
{
    public class HeroTests
    {
        [Fact]
        public void CharacterIsLevelOneWhenCreated_ShouldReturnTrue()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Smasher");
            int expected = 1;
            //Act
            int actual = testWarrior.Level;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_WhenCharacterGainsLevelItShouldIncrese_ShouldReturnTrue()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Smasher");
            int expected = 2;
            //Act
            testWarrior.LevelUp();
            //Assert
            Assert.Equal(expected, testWarrior.Level);
        }
        [Fact]
        public void StartsWith_WarriorStartsWithCorrectPrimaryAttributes_ShouldReturnTrue()
        {
            //Assert
            Warrior testWarrior = new Warrior("Smasher");
            PrimaryAttribute attributes = new PrimaryAttribute()
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
                Vitality = 10,
            };
            //Act
            PrimaryAttribute actual = testWarrior.PrimaryAttributes;
            var expectedPrimaryAttributes = new Likeness<PrimaryAttribute, PrimaryAttribute>(attributes);
            //Assert
            Assert.True(expectedPrimaryAttributes.Equals( actual));
        }
        [Fact]
        public void StartsWith_MageStartsWithCorrectPrimaryAttributes_ShouldReturnTrue()
        {
            //Assert
            Mage testMage = new Mage("Caster");
            PrimaryAttribute attributes = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
                Vitality = 5,
            };
            //Act
            PrimaryAttribute actual = testMage.PrimaryAttributes;
            var expectedPrimaryAttributes = new Likeness<PrimaryAttribute, PrimaryAttribute>(attributes);
            //Assert
            Assert.True(expectedPrimaryAttributes.Equals(actual));
        }
        [Fact]
        public void StartsWith_RogueStartsWithCorrectPrimaryAttributes_ShouldReturnTrue()
        {
            //Assert
            Rogue testRogue = new Rogue("Stabby");
            PrimaryAttribute attributes = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
                Vitality = 8,
            };
            //Act
            PrimaryAttribute actual = testRogue.PrimaryAttributes;
            var expectedPrimaryAttributes = new Likeness<PrimaryAttribute, PrimaryAttribute>(attributes);
            //Assert
            Assert.True(expectedPrimaryAttributes.Equals(actual));
        }
        [Fact]
        public void StartsWith_RangerStartsWithCorrectPrimaryAttributes_ShouldReturnTrue()
        {
            //Assert
            Ranger testRanger = new Ranger("Leg'O las");
            PrimaryAttribute attributes = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1,
                Vitality = 8,
            };
            //Act
            PrimaryAttribute actual = testRanger.PrimaryAttributes;
            var expectedPrimaryAttributes = new Likeness<PrimaryAttribute, PrimaryAttribute>(attributes);
            //Assert
            Assert.True(expectedPrimaryAttributes.Equals(actual));
        }
        [Fact]
        public void LevelUp_WarriorPrimaryAttributesIncreaseWhenLeveledUp()
        {
            //Assert
            Warrior testWarrior = new Warrior("Smasher");
            testWarrior.LevelUp();
            PrimaryAttribute attributes = new PrimaryAttribute()
            {
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2,
                Vitality = 15,
            };
            //Act
            PrimaryAttribute actual = testWarrior.PrimaryAttributes;
            var expectedPrimaryAttributes = new Likeness<PrimaryAttribute, PrimaryAttribute>(attributes);
            //Assert
            Assert.True(expectedPrimaryAttributes.Equals(actual));
        }
        [Fact]
        public void LevelUp_MagePrimaryAttributesIncreaseWhenLeveledUp()
        {
            //Assert
            Mage testMage = new Mage("Caster");
            testMage.LevelUp();
            PrimaryAttribute attributes = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13,
                Vitality = 8,
            };
            //Act
            PrimaryAttribute actual = testMage.PrimaryAttributes;
            var expectedPrimaryAttributes = new Likeness<PrimaryAttribute, PrimaryAttribute>(attributes);
            //Assert
            Assert.True(expectedPrimaryAttributes.Equals(actual));
        }
        [Fact]
        public void LevelUp_RoguePrimaryAttributesIncreaseWhenLeveledUp()
        {
            //Assert
            Rogue testRogue = new Rogue("Stabby");
            testRogue.LevelUp();
            PrimaryAttribute attributes = new PrimaryAttribute()
            {
                Strength = 4,
                Dexterity = 12,
                Intelligence = 2,
                Vitality = 16,
            };
            //Act
            PrimaryAttribute actual = testRogue.PrimaryAttributes;
            var expectedPrimaryAttributes = new Likeness<PrimaryAttribute, PrimaryAttribute>(attributes);
            //Assert
            Assert.True(expectedPrimaryAttributes.Equals(actual));
        }
        [Fact]
        public void LevelUp_RangerPrimaryAttributesIncreaseWhenLeveledUp()
        {
            //Assert
            Ranger testRanger = new Ranger("Leg'O las");
            testRanger.LevelUp();
            PrimaryAttribute attributes = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2,
                Vitality = 10,
            };
            //Act
            PrimaryAttribute actual = testRanger.PrimaryAttributes;
            var expectedPrimaryAttributes = new Likeness<PrimaryAttribute, PrimaryAttribute>(attributes);
            //Assert
            Assert.True(expectedPrimaryAttributes.Equals(actual));
        }
        [Fact]
        public void LevelUp_WarriorSecondaryAttributesIncreaseWhenLeveledUp()
        {
            //Assert
            Warrior testWarrior = new Warrior("Smasher");
            testWarrior.LevelUp();
            SecondaryAttribute attributes = new SecondaryAttribute()
            {
                Health = 150,
                Armor = 12,
                ElementalResistance = 2,
            };
            //Act
            SecondaryAttribute actual = testWarrior.SecondaryAttributes;
            var expectedSecondaryAttributes = new Likeness<SecondaryAttribute, SecondaryAttribute>(attributes);
            //Assert
            Assert.True(expectedSecondaryAttributes.Equals(actual));
        }
    }
}