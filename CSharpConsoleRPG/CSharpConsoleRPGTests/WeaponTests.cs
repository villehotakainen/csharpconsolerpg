using CSharpConsoleRPG.Attributes;
using CSharpConsoleRPG.Characters;
using CSharpConsoleRPG.Custom_Exceptions;
using CSharpConsoleRPG.Items;
using SemanticComparison;
using System;
using Xunit;

namespace CSharpConsoleRPGTests
{
    public class WeaponTests
    {
        [Fact]
        public void Equip_CharacterTriesToEquipHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Crusher");
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                LvlToUse = 2,
                EquipmentSlot = Enums.Slot.Weapon,
                WeaponType = Enums.WeaponType.Axe,
                WeaponAttributes = new WeaponAttribute() { Damage = 7, AttackSpeed = 1.1 }
            };
            //Act
            var caughtException = Assert.Throws<InvalidWeaponException>(() => testWarrior.CheckIfWeaponCanBeEquipped(testAxe));
            //Assert
            Assert.Equal("Your character level is too low to use this weapon", caughtException.Message);
        }
        [Fact]
        public void Equip_CharacterTriesToEquipHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Crusher");
            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                LvlToUse = 2,
                EquipmentSlot = Enums.Slot.Body,
                ArmorType = Enums.ArmorType.Plate,
                Attributes = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };
            //Act
            var caughtException = Assert.Throws<InvalidArmorException>(() => testWarrior.CheckIfArmorCanBeEquipped(testPlateBody));
            //Assert
            Assert.Equal("Your character level is too low to use wear this armor", caughtException.Message);
        }
        [Fact]
        public void Equip_CharacterTriesToEquipWrongTypeWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Crusher");
            Weapon testBow = new Weapon()
            {
                Name = "Common bow",
                LvlToUse = 1,
                EquipmentSlot = Enums.Slot.Weapon,
                WeaponType = Enums.WeaponType.Bow,
                WeaponAttributes = new WeaponAttribute() { Damage = 12, AttackSpeed = 0.8 }
            };
            //Act
            var caughtException = Assert.Throws<InvalidWeaponException>(() => testWarrior.CheckIfWeaponCanBeEquipped(testBow));
            //Assert
            Assert.Equal("You cannot use this weapon. You are able to use Staffs or Wands", caughtException.Message);
        }
        [Fact]
        public void Equip_CharacterTriesToEquipWrongTypeArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Crusher");
            Armor testClothHead = new Armor()
            {
                Name = "Common cloth head armor",
                LvlToUse = 1,
                EquipmentSlot = Enums.Slot.Head,
                ArmorType = Enums.ArmorType.Cloth,
                Attributes = new PrimaryAttribute() { Vitality = 1, Intelligence = 5 }
            };
            //Act
            var caughtException = Assert.Throws<InvalidArmorException>(() => testWarrior.CheckIfArmorCanBeEquipped(testClothHead));
            //Assert
            Assert.Equal("You cannot wear this armor. You are able to wear mail or plate armor", caughtException.Message);
        }
    }
}