﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpConsoleRPG.Attributes
{
    public class SecondaryAttribute
    {
        public int Health { get; set; }
        public int Armor { get; set; }
        public int ElementalResistance { get; set; }
    }
}
