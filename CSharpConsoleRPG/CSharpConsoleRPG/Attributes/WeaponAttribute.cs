﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpConsoleRPG.Attributes
{
    public class WeaponAttribute
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
    }
}
