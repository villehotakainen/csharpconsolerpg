﻿using CSharpConsoleRPG.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using static CSharpConsoleRPG.Items.Enums;

namespace CSharpConsoleRPG.Items
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttribute WeaponAttributes { get; set; }
        public double DamagePerSecond()
        {
            return WeaponAttributes.Damage * WeaponAttributes.AttackSpeed;
        }
    }
}
