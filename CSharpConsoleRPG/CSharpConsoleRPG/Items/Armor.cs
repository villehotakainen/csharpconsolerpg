﻿using CSharpConsoleRPG.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using static CSharpConsoleRPG.Items.Enums;

namespace CSharpConsoleRPG.Items
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public PrimaryAttribute Attributes { get; set; }
    }
}
