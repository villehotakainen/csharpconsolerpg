﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpConsoleRPG.Items
{
    public class Enums
    {
        public enum WeaponType
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }

        public enum ArmorType
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
        public enum Slot
        {
            Head,
            Body,
            Legs,
            Weapon
        }
    }
}
