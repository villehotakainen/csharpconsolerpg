﻿using System;
using System.Collections.Generic;
using System.Text;
using static CSharpConsoleRPG.Items.Enums;

namespace CSharpConsoleRPG.Items
{
    public class Item
    {
        public string Name { get; set; }
        public int LvlToUse { get; set; }
        public Slot EquipmentSlot { get; set; }
    }
}
