﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpConsoleRPG.Custom_Exceptions
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string message) : base(message)
        {
        }
    }
}
