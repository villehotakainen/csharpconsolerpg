﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpConsoleRPG.Custom_Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string message) : base(message)
        {
        }
    }
}
