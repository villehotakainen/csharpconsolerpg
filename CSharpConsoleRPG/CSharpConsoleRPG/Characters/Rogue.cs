﻿using CSharpConsoleRPG.Attributes;
using CSharpConsoleRPG.Custom_Exceptions;
using CSharpConsoleRPG.Items;
using System;
using System.Collections.Generic;
using System.Text;
using static CSharpConsoleRPG.Items.Enums;

namespace CSharpConsoleRPG.Characters
{
    public class Rogue : Hero
    {
        public Rogue(string name) : base(name)
        {
            // Specify staring attributes for Rogue class
            PrimaryAttributes = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
                Vitality = 8,
            };
            SecondaryAttributes = new SecondaryAttribute()
            {
                Health = PrimaryAttributes.Vitality * 10,
                Armor = PrimaryAttributes.Strength + PrimaryAttributes.Dexterity,
                ElementalResistance = PrimaryAttributes.Intelligence,
            };
        }
        // Calculate new primary and secondary attributes when Hero gains a level
        public override void LevelUp()
        {
            PrimaryAttribute RoguelevelUpStats = new PrimaryAttribute
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
                Vitality = 8,
            };
            PrimaryAttributes += RoguelevelUpStats;
            SecondaryAttributes = new SecondaryAttribute()
            {
                Health = PrimaryAttributes.Vitality * 10,
                Armor = PrimaryAttributes.Strength + PrimaryAttributes.Dexterity,
                ElementalResistance = PrimaryAttributes.Intelligence,
            };
        }
        // Check if weapon can be used and throw InvalidWeaponException if false
        public override void CheckIfWeaponCanBeEquipped(Weapon newWeapon)
        {
            if (newWeapon.WeaponType != WeaponType.Dagger && newWeapon.WeaponType != WeaponType.Sword)
                throw new InvalidWeaponException($"You cannot use this weapon. You are able to use Swords or Daggers");
            if (newWeapon.LvlToUse > Level)
                throw new InvalidWeaponException("Your character level is too low to use this weapon");
        }
        // Check if armor can be equipped and then throw InvalidArmorException if false
        public override void CheckIfArmorCanBeEquipped(Armor newArmor)
        {
            if (newArmor.ArmorType != ArmorType.Leather && newArmor.ArmorType != ArmorType.Mail)
                throw new InvalidArmorException($"You cannot wear this. You are able to wear leather or mail armor");
            if (newArmor.LvlToUse > Level)
                throw new InvalidWeaponException("Your character level is too low to use wear this armor");
        }
    }
}
