﻿using CSharpConsoleRPG.Attributes;
using CSharpConsoleRPG.Custom_Exceptions;
using CSharpConsoleRPG.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static CSharpConsoleRPG.Items.Enums;

namespace CSharpConsoleRPG.Characters
{
    public class Mage : Hero
    {
        public Mage(string name) : base(name)
        {
            // Specify staring attributes for Mage class
            PrimaryAttributes = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
                Vitality = 5,
            };
            // Calculate SecondaryAttributes based on PrimaryAttributes
            SecondaryAttributes = new SecondaryAttribute()
            {
                Health = PrimaryAttributes.Vitality * 10,
                Armor = PrimaryAttributes.Strength + PrimaryAttributes.Dexterity,
                ElementalResistance = PrimaryAttributes.Intelligence,
            };
        }
        // Calculate new primary and secondary attributes when Hero gains a level
        public override void LevelUp()
        {
            PrimaryAttribute MagelevelUpStats = new PrimaryAttribute
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 5,
                Vitality = 3,
            };
            PrimaryAttributes += MagelevelUpStats;
            SecondaryAttributes = new SecondaryAttribute()
            {
                Health = PrimaryAttributes.Vitality * 10,
                Armor = PrimaryAttributes.Strength + PrimaryAttributes.Dexterity,
                ElementalResistance = PrimaryAttributes.Intelligence,
            };
        }
        // Check if weapon can be used and throw InvalidWeaponException if false
        public override void CheckIfWeaponCanBeEquipped(Weapon newWeapon)
        {
            if (newWeapon.WeaponType != WeaponType.Wand && newWeapon.WeaponType != WeaponType.Staff)
                throw new InvalidWeaponException($"You cannot use this weapoon. You are able to use Staffs or Wands");
            if (newWeapon.LvlToUse > Level)
                throw new InvalidWeaponException("Your character level is too low to use this weapon");
        }
        // Check if armor can be equipped and then throw InvalidArmorException if false
        public override void CheckIfArmorCanBeEquipped(Armor newArmor)
        {
            if (newArmor.ArmorType != ArmorType.Cloth)
                throw new InvalidArmorException($"You cannot wear this armor. You are able to wear cloth armor");
            if (newArmor.LvlToUse > Level)
                throw new InvalidWeaponException("Your character level is too low to use wear this armor");
        }
    }
}
