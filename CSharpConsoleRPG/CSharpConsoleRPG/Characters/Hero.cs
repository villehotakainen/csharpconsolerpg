﻿using CSharpConsoleRPG.Attributes;
using CSharpConsoleRPG.Custom_Exceptions;
using CSharpConsoleRPG.Items;
using System;
using System.Collections.Generic;
using System.Text;
using static CSharpConsoleRPG.Items.Enums;

namespace CSharpConsoleRPG
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttribute PrimaryAttributes { get; set; }
        public SecondaryAttribute SecondaryAttributes { get; set; }
        public Weapon Weapon { get; set; }
        public Armor Armor { get; set; }
        public abstract void CheckIfWeaponCanBeEquipped(Weapon w);
        public abstract void LevelUp();
        public abstract void CheckIfArmorCanBeEquipped(Armor a);
        public StringBuilder printStats = new StringBuilder();

        // Dictionary that holds all Armor and Weapon objects
        public Dictionary<Slot, object> Inventory = new Dictionary<Slot, object>();

        public Hero(string name)
        {
            Name = name;
            Level = 1;
        }
        public void EquipWeapon(Weapon w)
        {
            try
            {
                CheckIfWeaponCanBeEquipped(w);

                AddWeaponToCorrectSlot(w);
            }
            catch (InvalidWeaponException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void EquipArmor(Armor a)
        {
            try
            {
                CheckIfArmorCanBeEquipped(a);
                AddArmorToCorrectSlot(a);
            }
            catch (InvalidWeaponException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        //Add armor class object to corresponding Dictionary slot
        public void AddArmorToCorrectSlot(Armor armor)
        {
            switch (armor.EquipmentSlot)
            {
                case Slot.Head:
                    Inventory.Add(Slot.Head, armor);
                    Console.WriteLine("New armor equipped!");
                    break;
                case Slot.Body:
                    Inventory.Add(Slot.Body, armor);
                    Console.WriteLine("New armor equipped!");
                    break;
                case Slot.Legs:
                    Inventory.Add(Slot.Legs, armor);
                    Console.WriteLine("New armor equipped!");
                    break;
                default:
                    throw new InvalidArmorException("Invalid slot");
            }
        }
        // Add Weapon object to weapon slot in Dictionary
        public void AddWeaponToCorrectSlot(Weapon weapon)
        {
            Inventory.Add(Slot.Weapon, weapon);
            Console.WriteLine("New weapon equipped!");
        }
        // Prints current character values to console
        public void PrintAttributes()
        {
            printStats.AppendLine("Character Name: " +Name).AppendLine("Character Level: "+Level.ToString()).Append('-', 6).Append("Primary Attributes")
                .Append('-',6).AppendLine(" ").AppendLine("Character Strength: "+PrimaryAttributes.Strength.ToString()).AppendLine("Character Dexterity: "+PrimaryAttributes.Dexterity.ToString())
                .AppendLine("Character Intelligence: "+PrimaryAttributes.Intelligence.ToString()).Append('-', 6).Append("Secondary Attributes").Append('-', 6).AppendLine(" ").AppendLine("Character Health: "+SecondaryAttributes.Health.ToString())
                .AppendLine("Character Armor: "+SecondaryAttributes.Armor.ToString()).AppendLine("Character ElementalResistance: "+SecondaryAttributes.ElementalResistance.ToString());
            Console.WriteLine(printStats);
        }
    }
}
