﻿using CSharpConsoleRPG.Attributes;
using CSharpConsoleRPG.Custom_Exceptions;
using CSharpConsoleRPG.Items;
using System;
using System.Collections.Generic;
using System.Text;
using static CSharpConsoleRPG.Items.Enums;

namespace CSharpConsoleRPG.Characters
{
    public class Warrior : Hero
    {

        public Warrior(string name) : base(name)
        {
            // Specify staring attributes for Warrior class
            PrimaryAttributes = new PrimaryAttribute()
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
                Vitality = 10,
            };
            SecondaryAttributes = new SecondaryAttribute()
            {
                Health = PrimaryAttributes.Vitality * 10,
                Armor = PrimaryAttributes.Strength + PrimaryAttributes.Dexterity,
                ElementalResistance = PrimaryAttributes.Intelligence,
            };
        }
        // Calculate new primary and secondary attributes when Hero gains a level
        public override void LevelUp()
        {
            Level += 1;
            PrimaryAttribute WarriorlevelUpStats = new PrimaryAttribute
            {
                Strength = 3,
                Dexterity = 2,
                Intelligence = 1,
                Vitality = 5,
            };
            PrimaryAttributes += WarriorlevelUpStats;
            SecondaryAttributes = new SecondaryAttribute()
            {
                Health = PrimaryAttributes.Vitality * 10,
                Armor = PrimaryAttributes.Strength + PrimaryAttributes.Dexterity,
                ElementalResistance = PrimaryAttributes.Intelligence,
            };
        }
        // Check if weapon can be used and throw InvalidWeaponException if false
        public override void CheckIfWeaponCanBeEquipped(Weapon newWeapon)
        {
            if (newWeapon.WeaponType != WeaponType.Axe && newWeapon.WeaponType != WeaponType.Sword
                && newWeapon.WeaponType != WeaponType.Hammer)
                throw new InvalidWeaponException("You cannot use this weapon. You are able to use Staffs or Wands");
            if (newWeapon.LvlToUse > Level)
                throw new InvalidWeaponException("Your character level is too low to use this weapon");
        }
        // Check if armor can be equipped and then throw InvalidArmorException if false
        public override void CheckIfArmorCanBeEquipped(Armor newArmor)
        {
            if (newArmor.ArmorType != ArmorType.Mail && newArmor.ArmorType != ArmorType.Plate)
                throw new InvalidArmorException("You cannot wear this armor. You are able to wear mail or plate armor");
            if (newArmor.LvlToUse > Level)
                throw new InvalidArmorException("Your character level is too low to use wear this armor");
        }
    }
}
